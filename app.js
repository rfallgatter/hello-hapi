'use strict';
const Hapi   = require('hapi');
const Server = new Hapi.Server();
const Hello  = require('./lib/hello');
const os = require( 'os' );

Server.connection({ port: 3000 });

Server.route({
    method: 'GET',
    path: '/hello/{user}',
    handler: function (request, reply) {
        console.log("user: "+decodeURIComponent(request.params.user));
        console.error("user error: "+decodeURIComponent(request.params.user));
        
        const result = Hello(decodeURIComponent(request.params.user)) + ' <br/>network: <br/>' + JSON.stringify(os.networkInterfaces());
        reply(result);
    }
});

// don't start server if this file was required

if (!module.parent) {

    Server.start((err) => {

        if (err) {
            throw err;
        }
        console.log(`Server running at: ${Server.info.uri}`);
    });
}

module.exports = Server;
